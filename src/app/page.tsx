import Contact from "@/components/examples/contact";
import FoterComponent from "@/components/examples/footer";
import HeroComponent from "@/components/examples/hero";
import HeroBottomComponent from "@/components/examples/hero-bottom";
import HeroSecondComponent from "@/components/examples/hero-second";
import TextParallaxContentExample from "@/components/examples/text-parallax-content";

export default function Page() {
  return (
    <section>
      <HeroComponent />
      <HeroSecondComponent />
      <HeroBottomComponent />
      <TextParallaxContentExample />
      <Contact />
      <FoterComponent />
    </section>
  );
}
