"use client";
import React from "react";
import Spotlight from "../ui/spotlight";

export default function HeroComponent() {
  return (
    <div
      className="h-screen w-full rounded-md flex md:items-center md:justify-center antialiased bg-grid-white/[0.02] relative overflow-hidden"
      id="hero"
    >
      <Spotlight
        className="-top-40 left-0 md:left-60 md:-top-20"
        fill="white"
      />
      <div className=" p-4 max-w-7xl  mx-auto relative z-10  w-full pt-20 md:pt-0">
        <h2 className="text-4xl md:text-7xl font-bold text-center bg-clip-text text-[--foreground-color] bg-gradient-to-b from-neutral-50 to-neutral-400 bg-opacity-50">
          FalcoTech <br /> Otro subtitulo
        </h2>
        <p className="mt-4 font-normal text-base text-[--foreground-color] max-w-lg text-center mx-auto">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam iusto
          possimus enim exercitationem dolorem eligendi quasi eveniet, nisi unde
          harum atque magnam cumque veniam voluptatibus? Fugit quaerat culpa ab
          maiores.
        </p>
      </div>
    </div>
  );
}
