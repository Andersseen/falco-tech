"use client";
import React from "react";
import { Vortex } from "../ui/vortex";

export default function HeroSecondComponent() {
  return (
    <div className="w-[calc(100%-4rem)] mx-auto rounded-md  h-screen overflow-hidden">
      <Vortex
        backgroundColor="black"
        rangeY={100}
        particleCount={100}
        baseHue={100}
        className="flex items-center flex-col justify-center px-2 md:px-10  py-4 w-full h-full"
      >
        <h2 className="text-4xl md:text-7xl font-bold text-center bg-clip-text text-[--foreground-color] bg-gradient-to-b from-neutral-50 to-neutral-400 bg-opacity-50">
          FalcoTech
        </h2>
        <p className="text-[--foreground-color] text-sm md:text-2xl max-w-xl mt-6 text-center">
          This is chemical burn. It&apos;ll hurt more than you&apos;ve ever been
          burned and you&apos;ll have a scar.
        </p>
      </Vortex>
    </div>
  );
}
