"use client";
import React from "react";
import { BackgroundBeams } from "../ui/background-beams";

export default function Contact() {
  return (
    <div className="h-[40rem] w-full rounded-md bg-black/[0.96] relative flex flex-col items-center justify-center antialiased">
      <div className="max-w-2xl mx-auto p-4">
        <h1 className="relative z-10 text-lg md:text-7xl  bg-clip-text text-[--foreground-color] bg-gradient-to-b from-neutral-200 to-neutral-600  text-center font-sans font-bold">
          Contacto
        </h1>
        <p></p>
        <p className="text-[--foreground-color] max-w-lg mx-auto my-2 text-sm text-center relative z-10">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione ex
          deleniti officiis. Dicta ab quo praesentium qui eveniet odio magni
          quisquam impedit mollitia distinctio delectus obcaecati sequi, eos
          corrupti adipisci.
        </p>
        <input
          type="text"
          placeholder="hi@manuarora.in"
          className="rounded-lg border border-neutral-800 focus:ring-2 focus:ring-teal-500  w-full relative z-10 mt-4  bg-neutral-950 placeholder:text-neutral-700"
        />
      </div>
      <BackgroundBeams />
    </div>
  );
}
